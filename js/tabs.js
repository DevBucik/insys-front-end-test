$(document).ready(function () {

    // Domy�lnie w��czona zak�adka .info
    $('.content div').hide();
    $('.info').show();

    // Bind'ujemy funkcje obs�ugi klikni�cia w pierwsz� ikon� nawigacyjn�
    // Po jej klikni�ciu chowamy wszystkie zak�adki po czym pokazujemy
    // zak�adk� ze znacznikiem o klasie .info
    $('.about_icon').click(function (e) {
        //make all tabs inactive
        $('.content div').hide();
        $('.info').show();
    });

    // Bind'ujemy funkcje obs�ugi klikni�cia w drug� ikon� nawigacyjn�
    // Po jej klikni�ciu wczytujemy miniatury zdj�� z serwisu Flickr do odpowiednik znacnzik�w <a> oraz <img>
    // a nast�pnie chowamy wszystkie zak�adki i pokazujemy zak�adk� ze znacznikiem o klasie .gallery
    $('.gallery_icon').click(function (e) {
        //make all tabs inactive

        var flickr_url = "https://api.flickr.com/services/feeds/photos_public.gne?format=json&tags=Marilyn,Monroe&jsoncallback=?";

        $.getJSON(flickr_url, function (data) {

            $(".gallery a").each(function (index) {
                $(this).find("img").attr("src", data.items[index].media['m']);
                $(this).attr("href", data.items[index].link);
            });

        });

        $('.content div').hide();
        $('.gallery').show();
    });
});